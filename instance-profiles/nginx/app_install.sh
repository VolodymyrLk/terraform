#!/usr/bin/env bash

echo "installing nginx"
apt-get update && install -y nginx
ufw allow 'Nginx HTTP'
echo "nginx success"