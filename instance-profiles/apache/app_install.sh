#!/usr/bin/env bash

echo ""
echo "------------------------------"
echo "installing apache"
apt-get update -y && apt-get upgrade -y
apt-get install -y apache2
echo "apache installation finished"

echo ""
echo "------------------------------"
echo "opening the firewall"
ufw allow 80
ufw status
ufw allow OpenSSH
ufw --force enable
echo "finished"