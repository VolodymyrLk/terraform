#!/usr/bin/env bash
echo "---------------"
echo "installing apache"
sudo apt-get update && apt-get -y install apache2
echo "apache success"
echo "ServerName $(ec2metadata --public-ipv4)" >> /etc/apache2/apache2.conf
sudo apache2ctl configtest
sudo systemctl restart apache2
sudo ufw app list
sudo ufw allow in "Apache Full"
sudo ufw status
echo "---------------"
echo "getting password"
sudo MYSQL_ROOT_PASS=$(aws secretsmanager get-secret-value --secret-id MyTestDatabaseSecret --output text --query 'SecretString')
echo "$MYSQL_ROOT_PASS"
echo "---------------"
echo "installing mysql-server and set the mysql root password"
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password $MYSQL_ROOT_PASS'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password $MYSQL_ROOT_PASS'
echo "---------------"
echo "noninteractive installing of phpmyadmin"
sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install mysql-server
echo "---------------"
echo "installing php"
sudo apt-get -y install php libapache2-mod-php php-mcrypt php-mysql
sudo sed -i '/DirectoryIndex/c\        DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm' /etc/apache2/mods-enabled/dir.conf
echo "---------------"
echo "testing if command sed set dir.conf correctly"
sudo cat /etc/apache2/mods-enabled/dir.conf
sudo systemctl restart apache2
echo "---------------"
echo "installing phpmyadmin"
sudo debconf-set-selections <<< 'phpmyadmin phpmyadmin/dbconfig-install boolean true'
sudo debconf-set-selections <<< 'phpmyadmin phpmyadmin/app-password-confirm password $MYSQL_ROOT_PASS'
sudo debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/admin-pass password $MYSQL_ROOT_PASS'
sudo debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/app-pass password $MYSQL_ROOT_PASS'
sudo debconf-set-selections <<< 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2'
echo "---------------"
echo "noninteractive installing of phpmyadmin"
sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install phpmyadmin php-mbstring php-gettext
sudo phpenmod mcrypt
sudo phpenmod mbstring
sudo systemctl restart apache2
echo "---------------"
echo "everything done"