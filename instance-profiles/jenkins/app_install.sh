#!/usr/bin/env bash

echo ""
echo "------------------------------"
echo "update and upgrade"
apt-get update -y && apt-get upgrade -y
echo "update and upgrade finished"

echo ""
echo "------------------------------"
echo "openjdk-8-jdk installation"
apt-get install openjdk-8-jdk -y
java -version
echo "openjdk-8-jdk finished"

echo ""
echo "------------------------------"
echo "jenkins installation"
wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
apt-get update -y
apt-get install -y jenkins
systemctl start jenkins
systemctl status jenkins
echo "jenkins finished"

echo ""
echo "------------------------------"
echo "opening the firewall"
ufw allow 8080
ufw status
ufw allow OpenSSH
ufw --force enable
echo "finished"

echo ""
echo "------------------------------"
echo "awscli installation"
apt-get install awscli -y
echo "awscli finished"