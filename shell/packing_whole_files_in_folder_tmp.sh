#!/usr/bin/env bash
echo "start script"
base=$(basename $PWD)
datef=$(date +%F)
cd ..
echo "archiving all files in $base directory"
tar -czf $base-$datef.tar.gz $base
echo "moving $base-$datef.tar.gz archive to /tmp directory"
mv $base-$datef.tar.gz /tmp/