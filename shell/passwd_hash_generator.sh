#!/usr/bin/env bash

sudo yum install pwgen -y
pwgen 10 10 > pwgen

while read p; do
  echo $p | openssl passwd -1 -stdin >> pwgencycle
done < pwgen