#!/usr/bin/env bash

file=system_status
#date
curr_date=$(date)
echo -e "Date: " $curr_date > $file
echo "------------------------------" >> $file
#os type
os=$(uname -o)
echo -e "Operating System Type: " $os >> $file
echo "------------------------------" >> $file
#os
echo -e "Centos release: " >> $file
cat /etc/centos-release >> $file
echo "------------------------------" >> $file
#system uptime
tecuptime=$(uptime | awk '{print $3,$4}' | cut -f1 -d,)
echo -e "System Uptime Days/(HH:MM): " $tecuptime >> $file
echo "------------------------------" >> $file
#disk usage
df -h | grep 'Filesystem\|/dev/sda*' > /tmp/diskusage
echo -e "Disk Usages: " >> $file
cat /tmp/diskusage >> $file
echo "------------------------------" >> $file
#ram and swap usage
free -h | grep -v + > /tmp/ramcache
echo -e "Ram Usages: " >> $file
cat /tmp/ramcache | grep -v "Swap" >> $file
echo -e "Swap Usages: " >> $file
cat /tmp/ramcache | grep -v "Mem" >> $file