#!/usr/bin/env bash
# set debug mode
set -x

# output log of userdata to /var/log/user-data.log
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
# create and put some content in the file
touch /home/ubuntu/created_by_userdata.txt
(
cat << 'EOP'
Hey there!!!
EOP
) > /home/ubuntu/created_by_userdata.txt

# install aws ec2 application

apt-get update
curl -O https://bootstrap.pypa.io/get-pip.py
apt install -y python
python get-pip.py --user
export PATH=~/.local/bin:$PATH
source ~/.bash_profile

pip install awscli

export AWS_ACCESS_KEY_ID=XXXXXXXXXXXXXXXXXXXXXX
export AWS_SECRET_ACCESS_KEY=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
export AWS_DEFAULT_REGION=eu-central-1

INSTANCE_ID=$(ec2metadata --instance-id)

TAG_VALUE=$(aws ec2 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" --output text | grep Role | awk '{print $5}')

echo $TAG_VALUE

# check if required installation file exists
exists=$(aws s3 ls instance-profiles/$TAG_VALUE/app_install.sh)
if [ -z "$exists" ]; then
  echo "app_install.sh file for $TAG_VALUE applicatoin does not exist"
  exit 1
else
  echo "app_install.sh file for $TAG_VALUE application exists"
fi

# copying file locally
aws s3 cp s3://instance-profiles/$TAG_VALUE/app_install.sh /usr/local/bin/app_install.sh

cd /usr/local/bin/
chmod +x app_install.sh

# run installation
bash app_install.sh