provider "aws" {
        access_key = "XXXXXXXXXXXXXXXX"
        secret_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        region = "eu-central-1"
}

resource "aws_instance" "example" {
        ami = "ami-7c412f13"
        instance_type = "t2.micro"
        key_name = "jenkins"
        security_groups= ["jenkins"]
        tags = {
          Name = "terraform-instance"
          Role = "apache"
        }
        user_data = "${file("userdata.sh")}"
}